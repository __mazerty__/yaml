package fr.mazerty.yaml

import android.content.Intent

fun createMapFromString(string: String): Map<String, Int> = string
    .split("\n")
    .map { it.split(";") }
    .filter { runCatching { it[1].toInt() }.isSuccess }
    .associate { it[0] to it[1].toInt() }

fun createStringFromMap(map: Map<String, Int>): String = map
    .map { it.key + ";" + it.value }
    .joinToString("\n")

fun Map<String, Int>.clean(): Map<String, Int> = this
    .filter { it.value >= 0 }
    .keys
    .groupBy { this[it]!! }
    .entries
    .sortedBy { it.key }
    .map { it.value }
    .flatMapIndexed { index: Int, strings: List<String> -> strings.map { it to index } }
    .toMap()

fun generatePagerContent(pkgs: List<Pkg>, promotedPkgs: Map<String, Int>, hiddenPkgs: List<String>): List<List<Pkg>> = buildList {
    add(emptyList()) // home screen
    pkgs
        .filter { it.name !in hiddenPkgs }
        .sortedBy { it.label }
        .groupBy { promotedPkgs[it.name] ?: -1 }.entries
        .sortedByDescending { it.key }
        .map { add(it.value) }
}

data class Pkg(
    val label: String,
    val name: String,
    val intent: Intent? = null
)
