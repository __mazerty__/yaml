package fr.mazerty.yaml

import android.content.Intent
import android.content.Intent.ACTION_MAIN
import android.content.Intent.CATEGORY_LAUNCHER
import android.content.pm.PackageManager.MATCH_ALL
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.BackHandler
import androidx.activity.compose.setContent
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.layout.Arrangement.Center
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.pager.HorizontalPager
import androidx.compose.foundation.pager.rememberPagerState
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color.Companion.Transparent
import androidx.compose.ui.window.Dialog
import fr.mazerty.yaml.R.string.demote
import fr.mazerty.yaml.R.string.hide
import fr.mazerty.yaml.R.string.promote
import fr.mazerty.yaml.R.string.refresh
import fr.mazerty.yaml.R.string.reset
import kotlinx.coroutines.launch
import java.io.File
import androidx.compose.ui.res.stringResource as s

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent { Launcher() }
    }

    @OptIn(ExperimentalFoundationApi::class)
    @Composable
    fun Launcher() {
        val pkgs = object {
            var state by remember { mutableStateOf(fetchPackages()) }

            fun refresh() {
                state = fetchPackages()
            }

            private fun fetchPackages(): List<Pkg> = packageManager
                .queryIntentActivities(Intent(ACTION_MAIN).addCategory(CATEGORY_LAUNCHER), MATCH_ALL)
                .map {
                    Pkg(
                        label = it.loadLabel(packageManager).toString(),
                        name = it.activityInfo.packageName,
                        intent = packageManager.getLaunchIntentForPackage(it.activityInfo.packageName)
                    )
                }
        }

        val promoted = object {
            private val file = File(filesDir, "promoted")
            var state by remember { mutableStateOf(if (file.exists()) createMapFromString(file.readText()) else emptyMap()) }

            fun canPromote(pkg: Pkg) = simulateXmote(pkg, +1)
            fun canDemote(pkg: Pkg) = simulateXmote(pkg, -1)

            private fun simulateXmote(pkg: Pkg, delta: Int): Map<String, Int>? = state
                .plus(pkg.name to state.getOrDefault(pkg.name, -1) + delta)
                .clean()
                .let { if (it != state) it else null }

            fun save(map: Map<String, Int>) {
                state = map
                file.run { if (map.isEmpty()) delete() else writeText(createStringFromMap(map)) }
            }

            fun reset() {
                state = emptyMap()
                file.delete()
            }
        }

        val hidden = object {
            private val file = File(filesDir, "hidden")
            var state by remember { mutableStateOf(if (file.exists()) file.readLines() else emptyList()) }

            fun add(pkg: Pkg) {
                state += pkg.name
                file.writeText(state.joinToString("\n"))
            }

            fun reset() {
                state = emptyList()
                file.delete()
            }
        }

        val menu = object {
            var state by remember { mutableStateOf<Pkg?>(null) }

            fun hide() {
                state = null
            }
        }.apply {
            state?.let { pkg ->
                Dialog(onDismissRequest = { hide() }) {
                    Column {
                        promoted.canPromote(pkg)?.let { Button(s(promote), onClick = { promoted.save(it); hide() }) }
                        promoted.canDemote(pkg)?.let { Button(s(demote), onClick = { promoted.save(it); hide() }) }
                        Button(s(hide), onClick = { hidden.add(pkg); hide() })
                        Divider()
                        Button(s(refresh), onClick = { pkgs.refresh(); hide() })
                        Button(s(reset), onClick = { promoted.reset(); hidden.reset(); hide() })
                    }
                }
            }
        }

        val pagerContent = generatePagerContent(pkgs.state, promoted.state, hidden.state)
        val pagerState = rememberPagerState(pageCount = { pagerContent.size })

        MaterialTheme {
            Surface(Modifier.fillMaxSize(), color = Transparent) {
                HorizontalPager(state = pagerState) { index ->
                    Column(Modifier.verticalScroll(rememberScrollState()), verticalArrangement = Center) {
                        pagerContent[index].forEach { pkg ->
                            Button(
                                text = pkg.label,
                                onClick = { startActivity(pkg.intent) },
                                onLongClick = { menu.state = pkg })
                        }
                    }
                }
            }
        }

        rememberCoroutineScope().apply {
            BackHandler {
                launch { pagerState.animateScrollToPage(0) }
            }
        }
    }
}
