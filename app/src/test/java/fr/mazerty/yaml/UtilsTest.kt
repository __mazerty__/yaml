package fr.mazerty.yaml

import org.junit.Assert.assertEquals
import org.junit.Test

val map = mapOf(
    "fr.mazerty.alpha" to 0,
    "fr.mazerty.beta" to 1,
    "fr.mazerty.gamma" to 2
)
val string = """
    fr.mazerty.alpha;0
    fr.mazerty.beta;1
    fr.mazerty.gamma;2
""".trimIndent()

class CreateMapFromStringTests {
    @Test
    fun nominal() {
        assertEquals(map, createMapFromString(string))
    }

    @Test
    fun oneLine() {
        assertEquals(mapOf("alpha" to 0), createMapFromString("alpha;0"))
    }

    @Test
    fun resilient() {
        assertEquals(emptyMap<String, Int>(), createMapFromString(""))
        assertEquals(emptyMap<String, Int>(), createMapFromString("alpha;error"))
        assertEquals(emptyMap<String, Int>(), createMapFromString("lorem ipsum"))
    }
}

class CreateStringFromMapTests {
    @Test
    fun nominal() {
        assertEquals(string, createStringFromMap(map))
    }

    @Test
    fun oneLine() {
        assertEquals("alpha;0", createStringFromMap(mapOf("alpha" to 0)))
    }

    @Test
    fun resilient() {
        assertEquals("", createStringFromMap(emptyMap()))
    }
}

class CleanTests {
    @Test
    fun nominal() {
        assertEquals(
            mapOf("alpha" to 0, "beta" to 1, "gamma" to 1, "delta" to 2),
            mapOf("alpha" to 1, "beta" to 3, "gamma" to 3, "delta" to 6, "epsilon" to -1).clean()
        )
    }

    @Test
    fun unchanged() {
        assertEquals(
            mapOf("alpha" to 0, "beta" to 1, "gamma" to 1, "delta" to 2),
            mapOf("alpha" to 0, "beta" to 1, "gamma" to 1, "delta" to 2).clean()
        )
    }

    @Test
    fun oneLine() {
        assertEquals(mapOf("alpha" to 0), mapOf("alpha" to 0).clean())
    }

    @Test
    fun resilient() {
        assertEquals(emptyMap<String, Int>(), emptyMap<String, Int>().clean())
    }
}

val alpha = Pkg("alpha", "fr.mazerty.alpha")
val beta = Pkg("beta", "fr.mazerty.beta")
val gamma = Pkg("gamma", "fr.mazerty.gamma")
val delta = Pkg("delta", "fr.mazerty.delta")
val epsilon = Pkg("epsilon", "fr.mazerty.epsilon")
val zeta = Pkg("zeta", "fr.mazerty.zeta")
val eta = Pkg("eta", "fr.mazerty.eta")
val theta = Pkg("theta", "fr.mazerty.theta")

class GeneratePagerContentTests {
    @Test
    fun onFirstLaunch() {
        assertEquals(
            listOf(emptyList(), listOf(alpha, beta, gamma)),
            generatePagerContent(listOf(alpha, beta, gamma), emptyMap(), emptyList())
        )
    }

    @Test
    fun hiddenApps() {
        assertEquals(
            listOf(emptyList(), listOf(gamma)),
            generatePagerContent(listOf(alpha, beta, gamma), emptyMap(), listOf("fr.mazerty.alpha", "fr.mazerty.beta"))
        )
    }

    @Test
    fun levels() {
        assertEquals(
            listOf(emptyList(), listOf(alpha), listOf(beta), listOf(gamma)),
            generatePagerContent(listOf(alpha, beta, gamma), mapOf("fr.mazerty.alpha" to 2, "fr.mazerty.beta" to 1), emptyList())
        )
    }

    @Test
    fun nominal() {
        assertEquals(
            listOf(
                emptyList(),
                listOf(alpha),
                listOf(beta, delta, gamma),
                listOf(epsilon, eta, zeta)
            ),
            generatePagerContent(
                listOf(alpha, beta, gamma, delta, epsilon, zeta, eta, theta),
                mapOf(
                    "fr.mazerty.alpha" to 2,
                    "fr.mazerty.beta" to 1,
                    "fr.mazerty.gamma" to 1,
                    "fr.mazerty.delta" to 1
                ),
                listOf("fr.mazerty.theta")
            )
        )
    }

    @Test
    fun resilient() {
        assertEquals(
            listOf(emptyList<Pkg>()),
            generatePagerContent(emptyList(), emptyMap(), emptyList())
        )
    }
}
